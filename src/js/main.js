(()=>{
const genForm = $('#gen-form');
const genSelect = $('#gen-select');
const genSelDefault = genSelect.html();
const genURL = $('#gen-url');
const genType = $('#gen-type');
const genDesc = $('#gen-description');
const genRes = $('#gen-result');
const genSave = $('#gen-save');
const genDel = $('#gen-delete');
const genRem = $('#gen-remove');
const genClr = $('#gen-clear');
const genRst = $('#gen-restore');
const genExp = $('#gen-export');
const pathPrefix = document.querySelector('meta[name="pathprefix"]').content;
const md = new markdownit();
let json = {};
let session = {};

const getSession = ()=>{
	const ls = localStorage.getItem('savedResults');
	session = JSON.parse(ls) || {};
	for(const uuid in session){
		const list = addList({uuid, name: session[uuid].name});
		session[uuid].list.forEach((text)=>{
			const item = makeItem({text, uuid});
			list.append(item);
		});
	}
};

const saveSession = ()=>{
	for(const k in session){
		if(!session[k].list.length) delete session[k];
	}
	const str = JSON.stringify(session);
	localStorage.setItem('savedResults', str);
};

const loadDB = async (url = `${pathPrefix}gens/`)=>{
	const db = await fetch(url);
	const data = await db.json();
	genSelect.html(genSelDefault);
	data.forEach((g)=>{
		const group = $('<optgroup/>');
		group.prop('label', g.name);
		genSelect.append(group);
		g.gens.forEach(o=>{
			const opt = $('<option>');
			opt.val(o.slug);
			opt.text(o.name);
			group.append(opt);
		});
	});
};

const loadGen = async (input)=>{
	const data = await fetch(input);
	json = await data.json();
	genRes.empty();
	genDesc.html(md.render(json.description));
	genType.prop('disabled', false);
	genType.empty();
	json.output.select.forEach((s, i)=>{
		const e = $('<option>');
		e.val(i);
		e.text(s.name);
		genType.append(e);
	});
}

const addList = ({uuid, name})=>{
	const el = $('<ul>');
	el.prop('id', uuid);
	el.attr('data-name', name);
	genSave.append(el);
	return el;
};

const makeItem = ({uuid, text})=>{
	const e = $('<li>');
	e.attr('data-uuid', uuid);
	e.text(text);
	return e;
};

genExp.on('click', (e)=>{
	let md = '';
	for(const k in session){
		const list = session[k].list;
		if(!list.length) continue;
		md += `### ${session[k].name}\n\n- ${list.join('\n- ')}\n`;
	}
	const file = new Blob([md], {type: 'text/plain;charset=utf-8'});
	const a = document.createElement("a");
	const url = URL.createObjectURL(file);
	a.href = url;
	a.download = `GenRoll-${new Date().toISOString()}.md`;
	document.body.appendChild(a);
	a.click();
	setTimeout(()=>{
		document.body.removeChild(a);
		window.URL.revokeObjectURL(url);
	}, 0);
});

genRem.on('click', (e)=>{
	if(confirm("Remove generated text?")){
		for(const k in session){
			genDel.append($(`#${k}`).children());
			session[k].list.length = 0;
		}
	}
});

genRst.on('click', (e)=>{
	if(confirm("Restore all removed generated text?")){
		genDel.each((i, el)=>{
			const uuid = el.dataset.uuid;
			$(`#${uuid}`).append(el);
			session[uuid].list.push(el.textContent);
		});
	}
});

genClr.on('click', (e)=>{
	if(confirm("Clear all removed generated text?")){
		genDel.empty();
	}
});

genRes.on('click', (e)=>{
	if(e.target.nodeName !== 'LI') return;
	let list = $(`#${json.uuid}`);
	if(!list.length){
		list = addList(json);
		session[json.uuid] = {
			name: json.name,
			list: []
		}
	}
	const slist = session[json.uuid].list;
	const text = e.target.textContent;
	if(slist.includes(text)) return;
	const el = $(e.target).clone();
	list.append(el);
	slist.push(text);
});

genSave.on('click', (e)=>{
	if(e.target.nodeName !== 'LI') return;
	const list = session[e.target.dataset.uuid].list;
	const i = list.indexOf(e.target.textContent);
	list.splice(i, 1);
	genDel.append(e.target);
});

genDel.on('click', (e)=>{
	if(e.target.nodeName !== 'LI') return;
	const uuid = e.target.dataset.uuid;
	const el = $(`#${uuid}`);
	session[uuid].list.push(e.target.textContent);
	el.append(e.target);
});

genForm.on('submit', (e)=>{
	e.preventDefault();
	const data = new FormData(e.target);
	const obj = Object.fromEntries(data.entries());
	const out = GenRoll({...obj, json});
	genRes.empty();
	out.forEach((text)=>{
		const item = makeItem({text, uuid: json.uuid});
		genRes.append(item);
	});
});

genSelect.on('change', (e)=>{
	loadGen(e.target.value);
});

genURL.on({
	keydown: (e)=>{
		if(e.key !== 'Enter') return;
		e.preventDefault();
		e.target.value = e.target.value;
		genURL.trigger('blur').trigger('focus');
	},
	change: (e)=>{
		loadGen(e.target.value);
	}

});

addEventListener('beforeunload', (e)=>{
	saveSession();
});

getSession();
loadDB();
})();