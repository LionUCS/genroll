/**
 * @module gen/roll
 */
(()=>{
	const regex = {
		string_key: /{([\w:\/]+)}(?:%(0?\.\d{1,3}))?/g,
		captialize: /\|(\w+)\|/g,
		array_split: /:\s?/
	};
	const arraySum = (arr)=>arr.reduce((n, c)=>n + parseFloat(c), 0);
	const captialize = (str)=>`${str.charAt(0).toUpperCase()}${str.slice(1)}`;
	const strKey = (str)=>{
		const arr = str.split('/');
		const keys = arr[0].split(regex.array_split);
		if(arr.length === 1 && keys === 1) return str;
		let weights, weight_sum;
		if(arr[1]){
			weights = arr[1].split(regex.array_split);
			weight_sum = arraySum(weights);
		} else{
			weights = [keys.length];
			weight_sum = Math.pow(keys.length, 2);
		}
		for(let i = 0; i < keys.length; i++){
			if(
				i === keys.length - 1 ||
				Math.random() <= weights[i % weights.length] / weight_sum
			) return keys[i];
		}
	};
	const randPart = (str, obj)=>{
		return str.replace(regex.string_key, (_match, part, chance)=>{
			if(chance && Math.random() <= parseFloat(chance)) return '';
			const p = strKey(part);
			const k = Math.floor(Math.random() * obj[p].length);
			const s = obj[p][k];
			return s;
		});
	};
	/**
	 * @exports gen/roll
	 * @name GenRoll
	 * @param {Object} args
	 * @param {Object} args.json JSON Object Generator
	 * @param {Number} args.select Integer for selecting rules in the generator
	 * @param {Number} args.count Integer specifing how many to generate 
	 * @returns {Array[String]}
	 */
	window.GenRoll = ({json, select = 0, count = 1})=>{
		const rule = json.output.select[select].rule;
		const div = ~~(count / rule.length);
		let r = count % rule.length;
		const out = [];
		for(let i = 0; i < rule.length; i++) {
			const k = rule[i];
			let len = div + !!r;
			for(let j = 0; j < len; j++){
				const rul = strKey(k);
				let str = randPart(json.output.rules[rul], json.parts);
				str = str.replace(regex.captialize, (_match, part)=>captialize(part));
				out.push(captialize(str));
			}
			if(out.length === count) break;
			if(r > 0) --r;
		}
		return out;
	};
	/* if(typeof window === "object" && typeof window.document === "object") {
		window.GenRoll = GenRoll;
	} */
})();