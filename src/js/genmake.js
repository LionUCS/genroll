(()=>{
const KEY = {
	DATA: 'gen-form'
};
const delimiter = ',';
const form = {
	el: $('#gen-form'),
	uuid: $('#gen-uuid'),
	name: $('#gen-name'),
	description: $('#gen-description'),
	parts: $('#gen-parts'),
	rules: $('#gen-rules'),
	select: $('#gen-select')
};
const genRes = $('#gen-result');
const template = {
	parts: document.getElementById('gen-template-parts'),
	rules: document.getElementById('gen-template-rules'),
	select: document.getElementById('gen-template-select'),
	test: document.getElementById('gen-template-test')
};
/**
 * Reset form
 * @param {Boolean} newForm Is this a new form?
*/
const reset = (newForm = true)=>{
	form.el[0].reset();
	if(newForm) form.uuid.val(crypto.randomUUID());
	$('.gen-set').each((i, el)=>{
		while(el.firstChild){
			el.removeChild(el.firstChild);
		}
		if(newForm) addField(el);
	});
};
/**
 * Turn FormData to proper Object
 * @returns {Object} Formatted Object
 */
const rollup = ()=>{
	const obj = FormDataJson.toJson(form.el);
	const seperator = new RegExp(`${delimiter} ?`);
	for(const k in obj.parts){
		obj.parts[k] = obj.parts[k].split(seperator);
	}
	for(let i = 0; i < obj.output.select.length; i++){
		if(obj.output.select[i].rule){
			obj.output.select[i].rule = obj.output.select[i].rule.split(seperator);
		}
	}
	return obj;
};
/**
 * Populate form with data, from passed data
 * @param {String} data - JSON formated object
 */
const populate = (data)=>{
	// data = JSON.parse(data);
	form.uuid.val(data.uuid);
	form.name.val(data.name);
	form.description.val(data.description);
	const seperator = `${delimiter} `;
	for(const k in data.parts){
		const part = data.parts[k];
		const field = addField(form.parts[0], k);
		field.find('.gen-parts').val(part.join(seperator));
	}
	for(const k in data.output.rules){
		const rule = data.output.rules[k];
		const field = addField(form.rules[0], k);
		field.find('.gen-rule').val(rule);
	}
	for(let i = 0; i < data.output.select.length; i++){
		const sel = data.output.select[i];
		const field = addField(form.select[0]);
		const fields = setIndex(i, field);
		fields.name.val(sel.name);
		if(sel.rule){
			fields.rule.val(sel.rule.join(seperator));
		}
	}
};
/**
 * 
 * @param {Number} i Integer index to set name property
 * @param {HTMLDivElement} field Field element node
 * @returns {{name: HTMLInputElement, rule: HTMLInputElement}}
 */
const setIndex = (i, field)=>{
	const fields = {
		name: field.find('.gen-select-name'),
		rule: field.find('.gen-select-rule')
	};
	for(const key in fields){
		const el = fields[key];
		el.prop('name', el.prop('name').replace(/\[\d*\]/, `[${i}]`));
	}
	return fields;
};
/**
 * Save data to localStorage
 */
const save = ()=>{
	const data = rollup();
	localStorage.setItem(KEY.DATA, JSON.stringify(data));
};
/**
 * Generate random hexidecimal number
 * @param {Number} size - Integer size of generated hex
*/
const genHex = (size = 2)=>[...Array(size)].map(()=>Math.floor(Math.random() * 16).toString(16)).join('');
/**
 * Set hex number on input and span tag
 * @param {HTMLInputElement} input - Input element setting the hex number
 * @param {String} hex - Hex formatted String
*/
const setKey = (input, key)=>{
	const keys = input.name.split('[').map((c)=>c.replace(']', ''));
	keys[keys.length - 1] = key;
	input.name = keys.reduce((p, c, i)=>`${p}[${c}]`);
};
/**
 * Append a field entry
 * @param {HTMLFieldSetElement} set - Fieldset being appended to
 * @param {String} hex - Hex formatted String
 * @returns {HTMLDivElement} Field element
*/
const addField = (set, key = genHex(4))=>{
	const id = set.id.split('-');
	const el = $(template[id[id.length - 1]].content.firstElementChild.cloneNode(true));
	el.find('.gen-hex').each((i, e)=>{
		setKey(e, key);
		var keyInput = e.previousElementSibling;
		keyInput.value = key;
		keyInput.addEventListener('change', setFieldKey);
	});
	el.find('.gen-copy').on('click', copyId);
	el.find('.gen-del').on('click', removeField);
	set.appendChild(el[0]);
	return el;
};
/**
 * Event Function for changing Field Key Input
 * @param {Event} e
 */
const setFieldKey = (e)=>{
	setKey(e.target.nextElementSibling, e.target.value);
};
/**
 * Remove a selected field entry
 * @param {InputEvent} e
*/
const removeField = (e)=>{
	e.preventDefault();
	const el = e.target.closest('.gen-field');
	const set = el.parentNode;
	set.removeChild(el);
	if(set === form.select[0]){
		for(let i = 0; i < set.children.length; i++) {
			const field = set.children[i];
			setIndex(i, field);
		}
	}
};
/**
 * Copy Part Group or Rule to clipboard
 * @param {InputEvent} e
*/
const copyId = (e)=>{
	e.preventDefault();
	const set = e.target.closest('.gen-set');
	const field = e.target.closest('.gen-field');
	const el = field.querySelector('.gen-id');
	let text = el.textContent;
	if(set.id === 'gen-parts') text = `{${text}}`;
	navigator.clipboard.writeText(text);
};
/**
 * Read the file data and set localStorage
 * @param {File} file
 * @returns {Promise<JSON>}
 */
const handleFile = (file)=>{
	return new Promise((resolve, reject)=>{
		const reader = new FileReader();
		reader.onload = (e)=>{
			resolve(e.target.result);
		};
		reader.onerror = reject;
		reader.readAsText(file);
	});
};
/**
 * Save JSON Object data to a file
 * @param {String} data
 * @param {String} filename
 */
const saveFile = (data, filename)=>{
	const file = new Blob([data], { type: 'text/plain;charset=utf-8' });
	const a = document.createElement("a");
	const url = URL.createObjectURL(file);
	a.href = url;
	a.download = filename;
	document.body.appendChild(a);
	a.click();
	setTimeout(() => {
		document.body.removeChild(a);
		window.URL.revokeObjectURL(url);
	}, 0);
};
const warnBeforeLeave = ()=>true;

$('.gen-add').on('click', (e)=>{
	e.preventDefault();
	const set = e.target.closest('.gen-toolbar').nextElementSibling;
	const field = addField(set);
	if(set === form.select[0]){
		const i = set.childElementCount;
		setIndex(i, field);
	}
});

$('#gen-new').on('click', (e)=>{
	e.preventDefault();
	if(confirm("This will erase everything, with no recovery. Are you sure?")){
		reset();
	}
});

$('#gen-load').on('click', (e)=>{
	e.preventDefault();
	const f = document.createElement('input');
	f.type = 'file';
	f.addEventListener('change', async (e)=>{
		handleFile(f.files[0]).then((text)=>{
			const data = JSON.parse(text);
			localStorage.setItem(KEY.DATA, text);
			reset(false);
			populate(data);
		}).catch((err)=>{
			alert(`Error: ${err}`);
		});
	}, false);
	document.body.appendChild(f);
	f.click();
	setTimeout(() => {
		document.body.removeChild(f);
	}, 0);
});

$('#gen-save').on('click', (e)=>{
	e.preventDefault();
	save();
});

$('#gen-export-json').on('click', (e)=>{
	e.preventDefault();
	const obj = rollup();
	saveFile(JSON.stringify(obj, null, '\t'), `GenRoll-${obj.name}.json`);
});

$('#gen-export-11ty').on('click', (e)=>{
	e.preventDefault();
	const obj = rollup();
	const name = obj.name;
	obj.name = '{{ name }}';
	let data = `---\nname: ${name}\n---\n`;
	data += JSON.stringify(obj, null, '\t');
	saveFile(data, `GenRoll-${name}.liquid`);
});

form.el.on('submit', (e)=>{
	e.preventDefault();
	save();
});

$('#gen-test').on('submit', (e)=>{
	e.preventDefault();
	const json = rollup();
	genRes.empty();
	for(let select = 0; select < json.output.select.length; select++){
		const gen = GenRoll({select, count: 1, json});
		const el = $(template.test.content.firstElementChild.cloneNode(true));
		el.find('.rule').text(json.output.select[select].name);
		el.find('.text').text(gen);
		genRes.append(el);
	}
});

onbeforeunload = warnBeforeLeave;

const saved = localStorage.getItem(KEY.DATA);
if(saved) populate(JSON.parse(saved));
else reset();
})();