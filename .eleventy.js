const inspect = require("util").inspect;
const { EleventyHtmlBasePlugin } = require("@11ty/eleventy");
const sass = require('eleventy-sass');
const dir = {
	input: 'src',
	output: 'public'
};
module.exports = (eleventyConfig)=>{
	eleventyConfig.addPlugin(EleventyHtmlBasePlugin);
	eleventyConfig.addPlugin(sass, {
		compileOptions: {
			permalink: (contents, inputPath)=>(data)=>`${data.page.filePathStem.replace(/^\/sass\//, "/css/")}.css`
		}
	});

	eleventyConfig.addPassthroughCopy('src/img');
	eleventyConfig.addPassthroughCopy('src/js');
	eleventyConfig.addPassthroughCopy('src/favicon.svg');

	eleventyConfig.addFilter('debug', c => `<pre>${inspect(c)}</pre>`);
	eleventyConfig.addFilter("keys", obj => Object.keys(obj));
	eleventyConfig.addFilter('stringify', JSON.stringify);
	eleventyConfig.addFilter('json2css', o => o ? Object.keys(o).map(k => `${k}: ${o[k]}`).join('; ') : null);

	return {dir};
};
module.exports.dir = dir;